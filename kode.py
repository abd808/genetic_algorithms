import numpy as np
import random
from tabulate import tabulate
from client import get_errors
import random
import math
from file import mse
import json

data = {}

key='Sx4hB5mXCuADC9CEztE2jfzPeo51HGXSxWpElaSN5NiXsMFjtO'
least=100000000
greatest=-1
fd = open('log.txt', 'w')
fd2 = open('test.txt', 'w')
error_arr_size=5
def delta(t, y, T):             #returns the change in value to be used during mutation operation
    exp = 1 - (t/T)
    exp = math.pow(exp, 5)      #magnitude of delta reduces as the algorithm reaches closer to completion
    r = random.uniform(0.0, 1.0)
    r = math.pow(r, exp)
    return y*(1-r)

def mutate(gen_num,iteration_limit, prob, vector):                        # mutate a given vector
    arr=np.copy(vector) 
    for i in range(len(arr)):
        chance = random.uniform(0, 1)                                     # checks whether the element should be mutated
        if chance <= prob:
            fact = random.choice([0, 1])
            if fact == 0:
                arr[i] = arr[i] + delta(gen_num, 10 - arr[i], iteration_limit)
            else:
                arr[i] = arr[i] - delta(gen_num, arr[i] + 10, iteration_limit)
    return arr

    # print(fact)
    # print(type(arr))
    # print('hi')
    # print(tabulate(np.reshape(arr, (-1, 1))))
    # print(tabulate(arr))
    # print(tabulate(np.reshape(arr, (-1, 1))))
    # print
    # return arr

def crossover(points, vec1, vec2):  
    veca=np.copy(vec1)
    vecb=np.copy(vec2)
    pts = []                                            # array to store crossover endpoints
    pts.append(0)
    pts.append(11)
    for i in range(points):
        pts.append(random.randint(1, 10))
    pts = list(set(pts))
    
    pts.sort()
    # print(pts)
    for i in range(len(pts)-1):
        if i%2 == 0:
            for j in range(pts[i], pts[i+1]):            # alternately swapping the elements in the intervals
                temp = veca[j]
                veca[j] = vecb[j]
                vecb[j] = temp
    return veca,vecb
    # for
# print("abdu ez gae", file = fd)


base=[0.0 , -1.18529e-12 , -3.36083e-13 , 4.77448e-11 , -2.01814e-10 , -2.35962e-15 , 8.25049e-16 , 2.53146e-05  ,-2.25361e-06 , -1.38728e-08 , 1.01725e-09]
                                                        # starting vector



# base = np.array(base)
final_weights=[0.0, -1.45799022, -2.28980078,  4.62010753, -1.75214813, -1.83669770,  8.52944060,  2.29423303, -2.04721003, -1.59792834,  9.98214034]
final_weights=np.array(final_weights)           
# base = final_weights
best_error=-10                                          # least error is always positive, started as negative initially
population_size=18                                     # number of weight vectors in each population    
iteration_limit=300                                   # number of times a new population is created

population=np.zeros((population_size,11))               # holds our current population
mutation_probability =0.9                             # probability of any weight being mutated (high initially)
choose_from=8                                          # top number of parents to be breeded from
automatic_select=2                                      # number of fittest parents and children that automatically make it to the next round
gen_num=0                                               # which iteration we currently are on
crossover_probability=1                                 # how many of the next generation 
                                                       
                                                       # generating first population
for i in range (population_size):
    new_mem=mutate(gen_num+50,iteration_limit,mutation_probability,base)
    population[i]=np.copy(new_mem)
    
print('Initial population', file= fd)
print(tabulate(population, showindex= 'always'), file= fd)

# data['pop'] = population.tolist()
# data['ite_lim'] = iteration_limit
# data['run_no'] = gen_num
# data['ite_per_run'] = 30
# with open('data.json', 'w') as w_file:
#     json.dump(data, w_file)

with open("data.json", "r") as read_file:
    data = json.load(read_file)
# print(data)
# population = data['pop']
# population = np.array(population)
# iteration_limit = data['ite_lim']
# gen_num = data['run_no']
# population_errors = data['pop_err']
# population_errors = np.array(population_errors)
# print(population)

# , headers=[i for i in range()]
                                                        # finding error for each element in population and storingd it along with index number of population element

population_errors=np.zeros((population_size,error_arr_size))         # stores errors and indices in form[total error,index,error1,error2,error defined]


for i in range (population_size):
    # error_vec=get_errors(key,population[i].tolist())
    # population_errors[i]=[(error_vec[0]+error_vec[1]+abs(error_vec[1]-error_vec[0])),i,error_vec[0],error_vec[1],1]
    population_errors[i]=[mse(population[i]),i,0,0,1]

# print(population)
population_errors=population_errors[population_errors[:, 0].argsort()] # sorting by error
# print(population_errors)
best_vectors=np.zeros((10,11))                          # stores 10 best weight vectors
best_errors=np.zeros((10,3))                            # stores corresponding best errors in form [total,error1,error2]


best_performer=np.zeros((iteration_limit,4))

# cnt = gen_num + data['ite_per_run']
# while gen_num < cnt:
# i = 0
while gen_num < iteration_limit:

    
    mutation_probability=gen_num/iteration_limit        # assigning mutation probability - linearly increases
    crossover_probability=1-gen_num/iteration_limit     # assigning crossover probability - linearly decreases
    crossover_number=int(crossover_probability*population_size)     # how many children are to be created by crossover
    if (crossover_number%2!=0):
        crossover_number-=1
    
    next_gen=np.zeros((population_size,11))             # vector that holds our next generation
    next_gen_count=automatic_select
    next_gen_errors=np.zeros((population_size,error_arr_size))            # stores errors and indices in form[total error,index,error1,error2]

    for i in range (automatic_select):                                  # retaining top performing individuals
        next_gen_errors[i]=np.copy(population_errors[i])
        next_gen_errors[i][1]=i
        next_gen[i]=np.copy(population[int(population_errors[i][1])])

    choose_from=int(population_size/2)                        
    cool_pop=population_errors[0:choose_from,0:]             # choosing top half fittest parents
    # print(cool_pop)
    # gen_num+=1
    if(crossover_number+automatic_select>population_size):      # decreasing crossover number in case number of retained children gets exceeded
        diff=(automatic_select+crossover_number)-population_size
        crossover_number-=diff

    children=np.zeros((crossover_number,11))                 # contains all the children from this gen
    number_of_children=0

 
   

    while(number_of_children<crossover_number):
    # print(cool_pop)
        pos1=random.randint(0,choose_from-1)                 # choosing two random parents
        pos2=pos1                                            # picking two parents to cross
        while(pos2==pos1):                                   # making sure disctinct members are picked
            pos2=random.randint(0,choose_from-1)
        # print(population_errors[pos1])
        parent1=population[int(cool_pop[pos1][1])]
        parent2=population[int(cool_pop[pos2][1])]           # using stored index to reach weight array 
        child1,child2=crossover(2,parent1,parent2)           # calling crossover
        children[number_of_children]=np.copy(child1)         # add to children list
        number_of_children+=1
        children[number_of_children]=np.copy(child2)
        number_of_children+=1
    
    number_of_children=0                        
    while(number_of_children<crossover_number):                 #filling next pop array with newly crossed children
        next_gen[next_gen_count]=np.copy(children[number_of_children])
        number_of_children+=1
        next_gen_count+=1

    from_parents=automatic_select
    while(next_gen_count<population_size):                      #filling the rest of the slots from the next pop array with the top performers of the current population
        next_gen[next_gen_count]=np.copy(population[int(population_errors[from_parents][1])])
        from_parents+=1
        next_gen_count+=1

    print("After Crossover",file=fd2)
    print(tabulate(next_gen),file=fd2)

    for i in range (population_size):
        if(next_gen_errors[i][4]==1 and next_gen_errors[i][0]<10):
            continue
        next_gen[i]=np.copy(mutate(gen_num,iteration_limit,mutation_probability,next_gen[i]))   

    for i in range (population_size):
        # error_vec=get_errors(key,next_gen[i].tolist())
        # next_gen_errors[i]=[(error_vec[0]+error_vec[1]+abs(error_vec[1]-error_vec[0])),i,error_vec[0],error_vec[1],1]
        next_gen_errors[i]=[mse(next_gen[i]),i,0,0,1]

    print("After Mutation",file=fd2)
    print(tabulate(next_gen),file=fd2)

    next_gen_errors=next_gen_errors[next_gen_errors[:, 0].argsort()] # sorting by error
    # print("Children Errors")
    # print(children_errors)
    
    for i in range(population_size):
        population_errors[i]=np.copy(next_gen_errors[i])
        population_errors[i][1]=i
        population[i]=np.copy(next_gen[int(next_gen_errors[i][1])])

    print("gen no "+ str(gen_num),file=fd2)
    print(tabulate(population),file=fd2)
    print("errors",file=fd2)
    print(tabulate(population_errors),file=fd2)

    best_performer[gen_num]=[gen_num,population_errors[0][0],population_errors[0][2],population_errors[0][3]]
    # print("gen no "+ str(gen_num),file=fd)
    # print(mse(population[0]),file=fd)
    gen_num+=1

for i in range (10):
    best_vectors[i]=np.copy(population[int(population_errors[i][1])])
    best_errors[i][0]=population_errors[i][0]
    best_errors[i][1]=population_errors[i][2]
    best_errors[i][2]=population_errors[i][3]*

data['pop'] = population.tolist()
data['ite_lim'] = iteration_limit
data['run_no'] = gen_num
data['ite_per_run'] = 30
data['pop_err'] = population_errors.tolist()
with open('data.json', 'w') as w_file:
    json.dump(data, w_file)

print(tabulate(best_performer), file= fd)
print(tabulate(best_vectors), file= fd)
print(tabulate(best_errors), file= fd)
print(tabulate(population), file= fd)
print(tabulate(population_errors), file= fd)


# a = [1.0,2.0,3.0,4.0,error_arr_size.0,6.0,7.0,8.0,9.0,10.0,11.0]
# b = [12.0,13.0,14.0,15.0,16.0,17.0,18.0,19.0,20.0,21.0,22.0]
# a,b = crossover(mutation_width,mutation_probability,3, a, b)
# print(a)
# print(b)